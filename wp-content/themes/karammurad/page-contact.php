<?php get_header(); ?>

		<section id="contact_map" class="delay1">
		</section>
		<section class="about-content delay1">
			<div class="about-overlay"></div>
			<div class="k-container-lg">
                <div class="contact-desc showMobile">
                    <h1 class="contact-head">We'd love to hear<br>from you</h1>
                    <h2 class="contact-desc">Contact us so we can help you better</h2>
                    <div class="contact-brief"><?php the_content(); ?></div>
                </div>
				<div class="contact-form-container clearfix">
					<div class="contact-form">
						YOUR NAME (REQUIRED)
						<input id="name" type="text">
						YOUR EMAIL (REQUIRED)
						<input id="email" type="text">
						SUBJECT
						<input id="subject" type="text">
						YOUR MESSAGE
						<textarea id="message"></textarea>
                        <div id="send_btn" class="contact-button">Send</div>
                        <div class="alert-text"></div>
					</div>
					<div class="loading-response">
						<div id="loading-center">
			                <div id="loading-center-absolute">
			                    <div class="object" id="object_four"></div>
			                    <div class="object" id="object_three"></div>
			                    <div class="object" id="object_two"></div>
			                    <div class="object" id="object_one"></div>
			                </div>
			            </div>
					</div>

					<div class="contact-desc hideMobile">
                        <h1 class="contact-head">We'd love to hear<br>from you</h1>
                        <h2 class="contact-desc">Contact us so we can help you better</h2>
                        <div class="contact-brief"><?php the_content(); ?></div>
                    </div>
				</div>
				<?php include 'inc/logo-footer.php' ?>
			</div>
		</section>

<!-- <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?sensor=false"></script>
<script src="<?php echo get_template_directory_uri(); ?>/js/googleMapStyle.js"></script> -->
<?php get_footer(); ?>
