        </div>
        <script type="text/javascript">
            var base_url = '<?= get_template_directory_uri(); ?>';
            var site_url = '<?= site_url(); ?>';
        </script>
        <?php if(is_page('contact')){?>
            <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js"></script>
            <script src="<?php echo get_template_directory_uri(); ?>/js/googleMapStyle.js"></script>
        <?php } ?>
        <script src="<?php echo get_template_directory_uri(); ?>/js/main.min.js"></script>

        <?php if(is_page('portfolio')){ ?>
            <script type="text/javascript">
                (function(d, s, id){
                   var js, fjs = d.getElementsByTagName(s)[0];
                   if (d.getElementById(id)) {return;}
                   js = d.createElement(s); js.id = id;
                   js.src = "//connect.facebook.net/en_US/sdk.js";
                   fjs.parentNode.insertBefore(js, fjs);
                 }(document, 'script', 'facebook-jssdk'));
            </script>
        <?php } ?> 
        <?php wp_footer(); ?>

        <!-- analytics -->
        <script>
          (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
          (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
          m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
          })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

          ga('create', 'UA-76810083-1', 'auto');
          ga('send', 'pageview');
        </script>

    </body>
</html>
