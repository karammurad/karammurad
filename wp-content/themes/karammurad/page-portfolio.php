<?php get_header(); ?>
        <!-- <div class="share-btn">share</div> -->
		<section class="intro-container delay1">
			<div class="overlay"></div>
			<div class="intro-about-text">
                <h1 class="about-title">Portfolio</h1>
                <h2 class="about-desc">We believe good work worth sharing</h2>
            </div>
			<!-- <div class="portfolio-list">
				<ul>
					<li>portrait</li>
					<li>Life</li>
					<li>architecture</li>
					<li>landscape</li>
				</ul>
			</div> -->
		</section>
        
		<section class="photo-grid delay1">

                    <div id="lightgallery" class="photo-container">
                        <?php 
                             $args = array(
                                 'cat' => 'portfolio',
                                 'posts_per_page' => -1
                             );
                             query_posts( $args );

                                // The Loop
                                while ( have_posts() ) : the_post();
                                    $image_medium = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'medium' );
                                    $image_large = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'large' );
                                    $image_full = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'full' );
                                    ?>

                                    <div class="wow slideInUp grid__item grid__sizer"
                                    data-download-url="false"
                                    data-responsive="<?php echo $image_large[0] ?> 480"
                                    data-src="<?php echo $image_full[0] ?>"
                                    data-img="<?php echo $image_large[0] ?>"
                                    data-sub-html="<h4><?php the_title() ?></h4><?php the_content() ?>"
                                    data-title="<?php the_title() ?>"
                                    data-desc="<?php the_content() ?>">
                                        <div class="image-conatainer">
                                            <div class="image-overlay">
                                                <div class="desc-container">
                                                    <div class="cat"><?php the_title() ?></div>
                                                    <div class="icon flat flaticon-connection"></div>           
                                                    <!-- <div class="desc"><?php the_content() ?></div> -->
                                                </div>
                                            </div>
                                        </div>
                                        
                                        <img src="<?php echo $image_large[0] ?>">

                                    </div>
                        <?php

                                endwhile;

                                // Reset Query
                                wp_reset_query();
                        ?>
        			</div>
		</section>

<?php get_footer(); ?>
