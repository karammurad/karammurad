<?php get_header(); ?>

    <section class="main-bg">
        <div class="overlay"></div>
        <div class="text-container">
            <h1 class="title delay1 karam-animate fadeInLeft">dramatic measures</h1>
            <a href="<?php echo site_url('portfolio') ?>"><div class="explore-more-btn  delay1-5 karam-animate  fadeInRight">explore more</div></a>
        </div>
    </section>
    <?php include 'inc/social-links.php' ?>
<?php get_footer(); ?>
