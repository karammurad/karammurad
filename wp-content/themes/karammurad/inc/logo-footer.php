    <div class="wow slideInUp about-logo"><img src="<?php echo get_template_directory_uri(); ?>/img/logo.png"></div>
    <?php if(!is_page('services')){ ?>
    <div class="wow slideInUp about-logo-desc">
        Photography service based in Kuala Lumpur, Malaysia for all kind of portraits, fashion, wedding occasions, website portfolio, and commercial products.
    </div>
    <div class="wow slideInUp about-contact-details">
        Bukit Jalil 57000 Kuala Lumpur<br>
        +60-1139936753<br>
        <a href="mailto:info@karammurad.com">service@karammurad.com</a>
    </div>
    
    <?php } ?>
    <div class="wow slideInUp about-social-container">
        <a href="https://www.facebook.com/karammuradph" target="_blank"><div class="social-icons flaticon-facebook43"></div></a>
        <a href="https://twitter.com/hardsss4" target="_blank"><div class="social-icons flaticon-twitter35"></div></a>
        <a href="https://www.instagram.com/hardsss4/" target="_blank"><div class="social-icons flaticon-instagram12"></div></a>
        <a href="https://plus.google.com/u/0/b/106641013415858883983/106641013415858883983" target="_blank"><div class="social-icons flaticon-google109"></div></a>
    </div>
</div>
<div class="about-copyright">© 2016 kARAM MURAD. All Rights Reserved</div>
