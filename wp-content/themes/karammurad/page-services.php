<?php get_header(); ?>

		<section class="services">
			<div class="overlay"></div>
			<div class="content">
				<h1 class="header wow fadeInUp" data-wow-delay="0.3s">Services</h2>
				<div class="header-desc wow fadeInUp" data-wow-delay="0.6s">Our service is all about quality. We offer a variety of photography services that meet your needs.</div>
				
				<div class="k-container-lg clearfix">
					<div class="service-tile-container clearfix">
						<div class="service-tile wow fadeInUp" data-wow-delay=".8s">
							<div class="tile-img fashion"></div>
							<div class="tile-content">
								<div class="tile-title">Fashion</div>
								<div class="tile-desc">
									We provide high quality fashion photography and we will make sure that your collection will look amazing online.
								</div>
							</div>
						</div>
						<div class="service-tile wow fadeInUp" data-wow-delay=".9s">
							<div class="tile-img business"></div>
							<div class="tile-content">
								<div class="tile-title">Business</div>
								<div class="tile-desc">
									We can come to your location and cover your business team shots or office interior.
								</div>
							</div>
						</div>
						<div class="service-tile wow fadeInUp" data-wow-delay="1s">
							<div class="tile-img events"></div>
							<div class="tile-content">
								<div class="tile-title">events</div>
								<div class="tile-desc">
									We cover events from seminars, conerts to weddings.
								</div>
							</div>
						</div>
						<div class="service-tile occasions wow fadeInUp">
							<div class="tile-img occasions"></div>
							<div class="tile-content occasions">
								<div class="tile-title">Special occassions</div>
								<div class="tile-desc">
									We are all about helping our clients to document their special moments with great quality photos.
								</div>
							</div>
						</div>
					</div>
				</div>

				<?php include 'inc/logo-footer.php' ?>
			</div>


		</section>


<?php get_footer(); ?>
