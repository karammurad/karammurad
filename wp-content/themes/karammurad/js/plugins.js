// Avoid `console` errors in browsers that lack a console.
(function() {
    var method;
    var noop = function () {};
    var methods = [
        'assert', 'clear', 'count', 'debug', 'dir', 'dirxml', 'error',
        'exception', 'group', 'groupCollapsed', 'groupEnd', 'info', 'log',
        'markTimeline', 'profile', 'profileEnd', 'table', 'time', 'timeEnd',
        'timeline', 'timelineEnd', 'timeStamp', 'trace', 'warn'
    ];
    var length = methods.length;
    var console = (window.console = window.console || {});

    while (length--) {
        method = methods[length];

        // Only stub undefined methods.
        if (!console[method]) {
            console[method] = noop;
        }
    }
}());

$(document).ready(function() {

    // lightGallery
    $('#lightgallery').lightGallery({
        hideBarsDelay:2000,
        thumbnail:false,
        hash: false,
        speed:1000,
        zoom:false,
        // hash:false,
        closable:false,
        enableZoomAfter:1000
        // mode:'lg-fade'
    });
});
$(window).load(function(){
    // Masonry grid setup
    $("#lightgallery").masonry({
        itemSelector: ".grid__item",
        columnWidth: ".grid__sizer",
        gutter: 0,
        percentPosition: true
      });

    // wow.js init
    var wow = new WOW(
      {
        boxClass:     'wow',      // animated element css class (default is wow)
        animateClass: 'animated', // animation css class (default is animated)
        offset:       0,          // distance to the element when triggering the animation (default is 0)
        mobile:       false,       // trigger animations on mobile devices (default is true)
        live:         false,       // act on asynchronously loaded content (default is true)
        callback:     function(box) {
          // the callback is fired every time an animation is started
          // the argument that is passed in is the DOM node being animated
        },
        // scrollContainer: null // optional scroll container selector, otherwise use window
      }
    );
    wow.init();

})
// Place any jQuery/helper plugins in here.
