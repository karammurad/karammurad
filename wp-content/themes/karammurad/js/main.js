$(window).load(function(){
	$('#loading').fadeOut()
	$('body').css('overflow','auto')
    // animate header
    $('header').addClass('karam-animate').addClass('fadeInDown')


    $('#contact_map, .intro-container').addClass('karam-animate').addClass('fadeInDown')
    $('.about-content, .photo-grid').addClass('karam-animate').addClass('fadeInUp')
    $('.about-content, .photo-grid').addClass('karam-animate').addClass('fadeInUp')

    // animate landing page
    $('.main-bg .title').addClass('karam-animate').addClass('fadeInLeft');
    $('.explore-more-btn').addClass('karam-animate').addClass('fadeInRight');

    // animate social icons footer
    $('.social').addClass('karam-animate').addClass('fadeInRight');


})

$(document).ready(function(){
	$('.menu-nav-btn').click(function(){
		$('.nav-container').toggleClass('slide-left')
		$('.bars').toggleClass('active');
		$('body').toggleClass('body-overflow')
	})
	$('.menu-nav-btn.close').click(function(){
		$('.web-container, header').removeClass('slide-left')
		$('.menu-nav').removeClass('slide-left-menu-nav')
		$('.menu-nav-container').removeClass('slide-left-nav')
		// $('.menu-nav-btn.close').removeClass('menu-nav-btn-hide')
		$('.bars.main').removeClass('bars-main-active');
	})

	$('.menu-nav a').click(function(){
		$('#loading').fadeIn()
	})

    // parallax scrolling
    if($(window).width() > 767){
        // console.log(767)
        var body = $(window).scrollTop();
        var parallax = (body * 0.08) - 100;
        $('.about-content, .services').css({'background-position':'50% '+parallax+'px'})
        $(window).bind('scroll', function(){
            var body = $(window).scrollTop();
            var parallax = (body * 0.08) - 100;
            $('.about-content, .services').css({'background-position':'50% '+parallax+'px'})
        })
    }

    $('#send_btn').click(function(){
    	var name = $('#name').val();
    	var email = $('#email').val();
    	var subject = $('#subject').val();
		var msg = $('#message').val();
        var filter = /^[\w\-\.\+]+\@[a-zA-Z0-9\.\-]+\.[a-zA-z0-9]{2,4}$/;

        
        if(name == ''){
            $('.alert-text').html("Name is Required!");
            $('#name').focus();
            return false;
        }

        if(email == ''){
            $('.alert-text').html("Email is Required!");
            $('#email').focus();
            return false;
        }

        if ( !(filter.test(email)) ){
        	$('.alert-text').html("Invalid email!");
            $('#email').focus();
        	return false;
        }

        if(subject == ''){
            $('.alert-text').html("Subject is Required!");
            $('#subject').focus();
            return false;
        }

        if(msg == ''){
            $('.alert-text').html("Message is Required!");
            $('#message').focus();
            return false;
        }
        
        else{
        	// console.log(site_url)
        	$('.loading-response').show();
        	$.ajax({
        		type:'POST',
        		url:site_url+'/ajax',
        		// datatype:'json',
        		data:{
        			fm_name:name,
        			fm_email:email,
        			fm_subject:subject,
        			fm_message:msg
        		},
        		success: function(res){
        			console.log(res)
        			$('.alert-text').html(res);
        			$('.contact-form input, .contact-form textarea').val('');
        			$('.loading-response').hide();
        		},
        		error: function(res){
        			console.log(res)
        		}
        	})

		}

		
	});

    // explore more event
    $('.explore-more-btn').click(function(){
        ga('send', {
          hitType: 'event',
          eventCategory: 'Reach',
          eventAction: 'Click',
          eventLabel: 'Explore more'
        });
    })

    // send button event
    $('.contact-form .contact-button').click(function(){
        ga('send', {
          hitType: 'event',
          eventCategory: ' Contact form',
          eventAction: 'Click',
          eventLabel: 'Send'
        });
    })

})

function parallax(){
	// var body = $('body').scrollTop();
    var body = $(window).scrollTop();
	if($(document).width() > 1024 ){
		var translate = body * 0.8;

		$('.main-bg').css({
		  '-webkit-transform' : 'translateY(0px, ' + translate + 'px)',
		  '-moz-transform'    : 'translateY(0px, ' + translate + 'px)',
		  '-ms-transform'     : 'translateY(0px, ' + translate + 'px)',
		  '-o-transform'      : 'translateY(0px, ' + translate + 'px)',
		  'transform'         : 'translateY(0px, ' + translate + 'px)'
		});
	}
}
