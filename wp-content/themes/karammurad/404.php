<?php get_header(); ?>

    <section class="main-bg wow fadeIn" data-wow-duration="1s" data-wow-delay="0.5s" style="background-image:url(<?php echo get_template_directory_uri(); ?>/img/sliders/landing.jpg)">
        <div class="overlay"></div>
        <div class="text-container">
            <h1 class="title wow fadeInLeft" data-wow-duration="1s" data-wow-delay="1s">404<br>page not found</h1>
            <a href="<?php echo site_url('portfolio') ?>"><div class="explore-more-btn  wow fadeInRight" data-wow-duration="1s" data-wow-delay="1.5s">explore more</div></a>
        </div>
    </section>
    <?php include 'inc/social-links.php' ?>
<?php get_footer(); ?>

