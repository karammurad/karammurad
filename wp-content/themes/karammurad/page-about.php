<?php get_header(); ?>

		<section class="intro-container about delay1">
			<div class="overlay"></div>
			<div class="intro-about-text">
				<h1 class="about-title">About me</h1>
				<h2 class="about-desc">Photographer & Web Developer</h2>
			</div> 
		</section>
		<section class="about-content delay1">
			<div class="about-overlay"></div>
			<div class="k-container">
				<div class="biography">
					<div class="img"></div>
					<h2 class="wow slideInUp title">Bio</h2>
                    <div class="wow slideInUp"><?php the_content() ;?></div>
				</div>

				<div class="wow slideInUp latest-work">
					<h2 class="title">latest work</h2>
					<div class="latest-work-container">
						<?php 
                             $args = array(
                                 'cat' => 'portfolio',
                                 'posts_per_page' => 3
                             );
                             query_posts( $args );
                                // The Loop
                                while ( have_posts() ) : the_post();
                                	$cat = get_the_category();
                                	foreach ($cat as $c) {
                                		if($c->name != "portfolio"){
                                			$cat = $c->name;
                                		}
                                	}
                                    $image_large = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'large' );
                        			?>
						<a href="<?php echo site_url('portfolio'); ?>">
							<div class="tile" style="background-image: url(<?php echo $image_large[0] ?>)">
								<div class="overlay">
									<div class="latest-work-text-container">
										<div class="sub-title"><?php echo $cat; ?></div>
										<div class="icon"></div>
									</div>
								</div>
							</div>
						</a>
						<?php
                                endwhile;

                                // Reset Query
                                wp_reset_query();
                        ?>
					</div>
				</div>

				<?php include 'inc/logo-footer.php' ?>
			</div>
		</section>

<?php get_footer(); ?>
