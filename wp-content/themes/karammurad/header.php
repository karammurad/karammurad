<!doctype html>
<html <?php language_attributes(); ?> class="no-js">
	<head>
		<meta charset="<?php bloginfo('charset'); ?>">
		<title><?php wp_title(''); ?></title>

		<link href="//www.google-analytics.com" rel="dns-prefetch">
        <link href="<?php echo get_template_directory_uri(); ?>/img/icons/favicon.ico" rel="shortcut icon">
        <link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/css/main.min.css">

		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">

        <link rel="shortcut icon" type="image/x-icon" href="<?php echo get_template_directory_uri(); ?>/favicon.ico">
		

		<?php wp_head(); ?>

	</head>
	<body <?php //body_class(); ?>>
    <body>
        <!--[if lt IE 8]>

        <![endif]-->
        <div id="loading">
            <div id="loading-center">
                <div id="loading-center-absolute">
                    <div class="object" id="object_four"></div>
                    <div class="object" id="object_three"></div>
                    <div class="object" id="object_two"></div>
                    <div class="object" id="object_one"></div>
                </div>
                <div class="loading-logo"><img src="<?php echo get_template_directory_uri(); ?>/img/logo.png"></div>
            </div>
        </div>
        <!-- <header class="wow fadeInDown" data-wow-duration="1s" data-wow-delay="2s"> -->
        <header class="delay<?php echo is_page() ? '' : '' ?>">
            <div class="logo showMobile"><img src="<?php echo get_template_directory_uri(); ?>/img/logo.png"></div>
            <div class="menu-nav-btn showMobile">
                <div class="bars top"></div>
                <div class="bars middle"></div>
                <div class="bars bottom"></div>
            </div>
            <div class="nav-container">
                <ul>
                    <a href="<?php echo site_url('services') ?>"><li class="nav-item <?php echo is_page('services') ? 'nav-active' : '' ?>">Services</li></a>
                    <a href="<?php echo site_url('portfolio') ?>"><li class="nav-item <?php echo is_page('portfolio') ? 'nav-active' : '' ?>">portfolio</li></a>
                    <li class="logo hideMobile"><div><img src="<?php echo get_template_directory_uri(); ?>/img/logo.png"></div></li>
                    <a href="<?php echo site_url('about') ?>"><li class="nav-item <?php echo is_page('about') ? 'nav-active' : '' ?>">about</li></a>
                    <a href="<?php echo site_url('contact') ?>"><li class="nav-item <?php echo is_page('contact') ? 'nav-active' : '' ?>">contact</li></a>
                </ul>
            </div>
        </header>
        <div class="">
